from setuptools import setup
setup(
    name='kms',
    version='1.3',
    description='Kms server',
    url='https://git.glebmail.xyz/PythonPrograms/kms_service',
    author='gleb',
    packages=['kms'],
    author_email='gleb@glebmail.xyz',
    license='GNU GPL 3',
    scripts=['bin/kms','bin/kms-registry']
)
