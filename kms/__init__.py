from pdsapi import *
import socket

ip = '127.0.0.1'
port = 4050
ipdb = '127.0.0.1'
portdb = 4011

db = pds(ipdb, portdb)
print(db.ping())
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sock.bind((ip, port))
sock.listen(10)

while True:
    conn, addr = sock.accept()
    key = conn.recv(2048)
    key = key.decode()
    accept = db.get(key)
    if key == accept:
        conn.send('OK'.encode())
    else:
        conn.send('NO'.encode())
